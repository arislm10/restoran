<div class="container" id="app">
  <h2>Data Maknanan</h2>
 
 <a class="btn btn-info" href="<?= site_url('Front_end/restoran/tambah_makanan') ?>"> Tambah </a>
 <br>
  <table class="table">
    <thead>
      <tr>
        <th>No</th>
        <th>Nama Menu</th>
        <th>Desc</th>
        <th>harga</th>
        <th>status</th>
        <th>Opsi</th>
      </tr>
    </thead>
    <tbody>
      <tr v-for="(m,id) in makanan  ">
      <td> {{id+1}}</td>
      <td> {{m.nama_makanan}}</td>
        <td>{{m.desc}}</td>
        <td>{{m.harga}}</td>
        <td>{{m.status}}</td>
        <td><a class="btn btn-default" :href=`<?= site_url('front_end/restoran/edit_makanan/') ?>${m.id_menu}`>Edit</a>||
        <a class="btn btn-default" :href=`<?= site_url('front_end/restoran/del/') ?>${m.id_menu}`>Hapus</a></td>
      </tr>
      
    </tbody>
  </table>
</div>
<script>
var app = new Vue({
  el: '#app',
    data: {
        makanan : [],
  },
  mounted(){
      this.getPesanan();
  },
  computed: {

  },
  methods:{
      getPesanan() {
      axios.get('http://localhost/restoran/index.php/Api/get_makanan/').then(
        result => {
          console.log(result.data);
          this.makanan = result.data;
        },
        error => {
          console.log(error);
        }
      );
    },
  }
})
</script>

