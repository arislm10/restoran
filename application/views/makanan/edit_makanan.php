<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Form Edit</h3>
								</div>
								<div class="panel-body">
								<form method="post" action="<?= site_url('back_end/admin/edit_proses') ?>">
                                <input type="hidden" name="id" value="<?= $this->uri->segment(4) ?>">
									<input type="text" name="menu" class="form-control" placeholder="Nama Menu" value="<?= $menu->nama_makanan ?>">
									<br>
									<input type="int" name="harga" class="form-control" placeholder="harga" value="<?= $menu->harga?>" >
									<br>
									<textarea name="deskripsi" class="form-control" placeholder="textarea" rows="4" placeholder="deskripsi"><?= $menu->desc?></textarea>
									<br>
									<select name="status" class="form-control">
                                    <option <?php if('ready' == $menu->status) { echo "selected"; } ?> value='ready'>ready</option>
                  <option <?php if('kosong' == $menu->status) { echo "selected"; } ?> value='kosong'>kosong</option>
									</select>

								</div>
				
							</div>
							<button type="submit" class="btn btn-primary">Simpan</button>
							</form>