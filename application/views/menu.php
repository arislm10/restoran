<div class="container" id="app">
    

<div class="menu-body">
   <!-- Section starts: Appetizers -->
   <div class="menu-section">
      <h3 class="menu-section-title">Daftar Makanan</h3>
      <!-- Item starts -->
      <div class="menu-item" v-for="(food,key) in makanan">
         <div class="menu-item-name">
          {{food.nama_makanan}}
         </div>
         <div class="menu-item-price">
            {{food.harga}}

         </div>
         <div class="menu-item-description">
            {{food.desc}}
         </div>
          </div>
   </div>
   <a class="btn btn-primary" href="<?= site_url('front_end/restoran/pesanan') ?>">Pesan Makanan</a>
      </div>
   </div>
   <script>
var app = new Vue({
  el: '#app',
    data: {
      // disable: false,
      makanan: [],  
  },
  mounted(){
      this.getFoods();
  },
  methods:{
      getFoods() {
      axios.get('http://localhost/restoran/index.php/Api/get_makanan/').then(
        result => {
          console.log(result.data);
          this.makanan = result.data;
          // this.pesanan = result.data;
        },
        error => {
          console.log(error);
        }
      );
    },
  }
})
</script>