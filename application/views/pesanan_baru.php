<div class="container" id="app">
    
<?php print_r($this->session->all_userdata());  ?>
<div class="menu-body">
   <!-- Section starts: Appetizers -->
   <div class="menu-section">
      <h3 class="menu-section-title">Pesanan Baru</h3>
      <!-- Item starts -->
      <div class="menu-item" v-for="(food,key) in makanan">
         <div class="menu-item-name">
          {{food.nama_makanan}}
         </div>
         <div class="menu-item-price">
            {{food.harga}}<button :disabled="buttonDisabled[key]" class="btn btn-default" @click="getMenu(key);buttonDisabled[key] = !buttonDisabled[key];makanan[key]['del'] = key">+</button>
         </div>
         <div class="menu-item-description">
            {{food.desc}}
         </div>
          </div>
   </div>

  <div class="form-group">
    <label for="email">Pesanan</label>
    <input type="text" class="form-control" id="email" v-model="generate" readonly>
  </div>
  <div class="form-group">
  <label for="meja">Nomor Meja</label>
  <select v-model="mejapilihan" class="form-control" id="sel1">
        <option v-for="mejas in meja">{{mejas}}</option>
</select>
  </div>
  <div class="form-group">
  <label for="meja">Makanan Yang di Pesan</label>
  <ul class="list-group" v-for="(pesan,key) in pesanan">
    <li class="list-group-item">{{pesan.nama_makanan}}<button @click="pesan.qty++">+</button>||
   <button @click="pesan.qty--">-</button> || <span>jumlah {{pesan.qty}}</span><button @click="deletePesanan(key)" style="color:white;position: relative;
    left: 226px;
" class="btn btn-danger">Hapus</button></li>
  </ul>
  </div>

  <button type="submit" @click="addPesanan()" class="btn btn-default">Submit</button>

      </div>
   </div>
<script>
var app = new Vue({
  el: '#app',
    data: {
      // disable: false,
      meja :[01,02,03,04,05],
      makanan: [],
      pesanan:[],
      total : 0,  
      mejapilihan:'',
      generate : '',
      urut: '',
      user :'<?php echo $this->session->userdata('user') ?>',
      buttonDisabled: []
  },
  mounted(){
      this.getFoods();
      this.countData();
  },
  methods:{
    deletePesanan(id) {
    this.buttonDisabled[this.pesanan[id].del] = false;
     this.pesanan.splice(id, 1);
    },
    addPesanan() {
      var formData = new FormData();
      formData.append('user', this.user);
      formData.append('kode', this.generate);
      formData.append('pesanan', JSON.stringify(this.pesanan));
      formData.append('meja', this.mejapilihan);
      axios
				.post('http://localhost/restoran/index.php/Api/addPesanan/', formData)
				.then(res => {
          location.reload();			
				})
				.catch(err => {
					console.log(err);
				});
		},
      getFoods() {
      axios.get('http://localhost/restoran/index.php/Api/get_makanan/').then(
        result => {
          console.log(result.data);
          this.makanan = result.data;
          // this.pesanan = result.data;
        },
        error => {
          console.log(error);
        }
      );
    },
     getMenu(id) {
      this.pesanan.push(this.makanan[id]);
      // this.pesanan[this.pesanan.length - 1]['jumlah'] = 1;
    },
    addJumlah(id) {
   this.pesanan[id]['jumlah'] += 1;
  //console.log(j);
    },
    generateDate(urut) {
      // var A = Date.now();
      // var newDate = 'A'.split('-').reverse().join(''); 
      var date = new Date();
      var day = date.getDate();
      var month = date.getMonth();
      var year = date.getFullYear();
      var hasil  = day + '' + month + '' + year;
      this.generate = 'ERP'+hasil+'-'+urut;
    },
    countData() {
      axios.get('http://localhost/restoran/index.php/Api/count').then(
        result => {
          let digit = result.data.jumlah.toString().length;
          let akhir = parseInt(result.data.jumlah) + 1;
          let no = 0;
          if (digit == 1) {
            no = '00'+akhir;
          } else if (digit == 2) {
            no = '0'+akhir;
          } else if (digit == 3) {
            no = akhir;
          }
          this.generateDate(no);
          // this.pesanan = result.data;
        },
        error => {
          console.log(error);
        }
      );
    }
  }
})
</script>
