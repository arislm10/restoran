<div class="container" id="app">
  <h2>Data Pesanan</h2>
 
  <table class="table">
    <thead v-for="">
      <tr>
        <td>kode Pesanan</td>
        <td>{{detailPesan.kode_pesanan}}</td>
      </tr>
      <tr>
        <td>No Meja</td>
        <td>{{detailPesan.no_meja}}</td>
      </tr>
      <tr> 
        <td>Makanan dan Minuman yang di pesan</td>
        <td><ul class="list-group">
        <li v-for="p in pesanan" class="list-group-item">{{p.nama_makanan}} <span>{{p.jumlah}}</span><span>x</span><span>{{p.harga}}</span> = <span>{{p.jumlah * p.harga}}</span></li>
        </ul></td>
      </tr>
      <tr>
        
      </tr>
      <tr>
        <td> Total Semua</td>
        <td>{{totals}}</td>
      </tr>
      
    </thead>
 
  </table>
  <button class="btn btn-primary"  @click="bayar(detailPesan.kode_pesanan)">Bayar</button>
</div>
<script>
var app = new Vue({
  el: '#app',
    data: {
      pesanan:[],
      detailPesan: '',
      total : 0,  
      user :'<?php echo $this->session->userdata('user') ?>',
  },
  mounted(){
      this.getPesanan();
  },
  computed: {
    totals() {
      let total = 0;
      let satuan = 0;
      this.pesanan.forEach(el => {
        satuan = parseInt(el.jumlah) * parseInt(el.harga);
        total = parseInt(total) + parseInt(satuan);
        
      });
      return total;
    }
  },
  methods:{
      
    bayar(k) {
      var data = {status : 'selesai',kode_pesanan : k};
      var formData = new FormData();
      formData.append('selesai', JSON.stringify(data));
      axios.post('http://localhost/restoran/index.php/Api/selesai/',formData).then(
        result => {
          alert('Terimakasih Telah Membayar');  
          window.location.replace("http://localhost/restoran/index.php/front_end/restoran/menu");
        })
        .catch(err => {
					console.log(err);
				});
    },
    getPesanan() {
      axios.get('http://localhost/restoran/index.php/front_end/restoran/tampil_pesan/'+'<?= $this->uri->segment(4) ?>').then(
        result => {
          console.log(result.data);
          this.pesanan= result.data;
          this.detailPesan = result.data[0];
          // this.pesanan = result.data;
        },
        error => {
          console.log(error);
        }
      );
    },
     getMenu(id) {
      this.pesanan.push(this.makanan[id]);
      // this.pesanan[this.pesanan.length - 1]['jumlah'] = 1;
    },
    getTotal() {
      // var total = this.jumlah *  
    }
  }
})
</script>

