<?php

if($this->session->userdata('user') == NULL) {
  redirect('back_end/admin/login');
}
?>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
<script src="<?= base_url('assets/vue.js') ?>"></script>
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,900" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oleo+Script" rel="stylesheet">
<style type="text/css">
.menu-body {
  max-width: 680px;
  margin: 0 auto;
  display: block;
  color: rgb(92, 92, 92);
}

.menu-section {
  margin-bottom: 80px;
}

.menu-section-title {
  font-family: georgia;
  font-size: 50px;
  display: block;
  font-weight:normal;
  margin: 20px 0; 
  text-align: Center;
}

.menu-item {
  margin: 35px 0;
  font-size: 18px;
}

.menu-item-name{
  font-family: helvetica;
  font-weight: bold;
  border-bottom: 2px dotted rgb(213, 213, 213);
}

.menu-item-description {
  font-style: italic;
  font-size: .9em;
  line-height: 1.5em;
}

.menu-item-price{
  float: right;
  font-weight: bold;
  font-family: arial;
  margin-top: -22px;
}

.menu-body {
  max-width: 680px;
  margin: 0 auto;
  display: block;
  color: rgb(92, 92, 92);
}
 
.menu-section {
  margin-bottom: 80px;
}
 
.menu-section-title {
  font-family: georgia;
  font-size: 50px;
  display: block;
  font-weight:normal;
  margin: 20px 0; 
  text-align: Center;
}
 
.menu-item {
  margin: 35px 0;
  font-size: 18px;
}
 
.menu-item-name{
  font-family: helvetica;
  font-weight: bold;
  border-bottom: 2px dotted rgb(213, 213, 213);
}
 
.menu-item-description {
  font-style: italic;
  font-size: .9em;
  line-height: 1.5em;
}
 
.menu-item-price{
  float: right;
  font-weight: bold;
  font-family: arial;
  margin-top: -22px;
}
</style>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?= site_url('front_end/restoran/menu') ?>">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= site_url('front_end/restoran/data_pesanan') ?>">Data Pesanan</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= site_url('front_end/restoran/data_activity') ?>">My activity</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= site_url('front_end/restoran/data_makanan') ?>">Data Makanan</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= site_url('back_end/admin/logout/') ?>">Logout</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
