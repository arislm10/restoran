<div class="container" id="app">
  <h2>Data Pesanan</h2>
 
  <table class="table">
    <thead>
      <tr>
        <th>No</th>
        <th>Nomor Meja</th>
        <th>kode Pesanan</th>
        <th>Status</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
      <tr v-for="(p,id) in pesanan">
      <td> {{id+1}}</td>
      <td> {{p.no_meja}}</td>
        <td>{{p.kode_pesanan}}</td>
        <td>{{p.status}}</td>
       <?php if($this->session->userdata('level') == 2){
         ?>
         <td><a class="btn btn-default" :href=`<?= site_url('front_end/restoran/bayar/') ?>${p.kode_pesanan}`>Bayar</a></td>
       <?php }else {
         echo "<td>kasir privilages</td>";
       } ?> 
      </tr>
      
    </tbody>
  </table>
</div>
<script>
var app = new Vue({
  el: '#app',
    data: {
        
        pesanan : [],
        makanan : [],
  },
  mounted(){
      this.getPesanan();
  },
  computed: {

  },
  methods:{
      getPesanan() {
      axios.get('http://localhost/restoran/index.php/Api/get_pesanan/').then(
        result => {
          console.log(result.data);
          this.pesanan = result.data;
        },
        error => {
          console.log(error);
        }
      );
    },
  }
})
</script>

