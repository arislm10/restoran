<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function dashboard()
	{
		$data['content'] = 'dashboard';
		$this->load->view('temp',$data);
	}
	public function login()
	{
		$this->load->view('login');
	}
	public function log()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, OPTIONS");
		$this->load->model('M_log');
		$id = $this->session->userdata('user');
		$log = $this->M_log->get_log($id)->result();
		echo json_encode($log);

	}
	public function tambah_menu()
	{
		$data['content'] = 'makanan/form_tambah';
		$this->load->view('temp',$data);
	}
	public function addProses()
	{
		$this->load->model('M_menu');
		$menu = $this->input->post('menu');
		$status = $this->input->post('status');
		$harga = $this->input->post('harga');
		$desc = $this->input->post('deskripsi');
		
		$data = array(
			'nama_makanan' => $menu,
			'status' => $status,
			'harga' => $harga,
			'desc' => $desc,
			'qty' => 1
		);
		$this->M_menu->insert($data);
		helper_log('menambah menu baru',$this->session->userdata('user'));
		redirect('Front_end/restoran/data_makanan');
	}
	public function edit_proses()
	{
		$this->load->model('M_menu');
		$menu = $this->input->post('menu');
		$id = $this->input->post('id');
		$status = $this->input->post('status');
		$harga = $this->input->post('harga');
		$desc = $this->input->post('deskripsi');
		
		$data = array(
			'nama_makanan' => $menu,
			'status' => $status,
			'harga' => $harga,
			'desc' => $desc,
		);
		$this->M_menu->edit($data,$id);
		helper_log('edit Makanan',$this->session->userdata('user'));
		redirect('Front_end/restoran/data_makanan');
	}
	function logout(){
		helper_log("Logout",$this->session->userdata('user'));
		$this->session->sess_destroy();
		echo json_encode('200 Ok');
		redirect('back_end/admin/login');
	  }


}
