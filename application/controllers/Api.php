<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {


  public function get_makanan()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, OPTIONS");
		$makanan = $this->db->get_where('tb_menu_makanan',array('status' => 'ready'))->result();
		echo json_encode($makanan);

	}
	public function addPesanan()
	{
		// header('Access-Control-Allow-Origin: *');
		// header('Content-Type: application/json');
		$meja = $this->input->post('meja');
		$user = $this->input->post('user');
		$kode = $this->input->post('kode');
		$datajson = $this->input->post('pesanan');
		$json = json_decode($datajson);
		foreach($json as $d) {
			$data = array(
				'no_meja' => $meja,
				'kode_pesanan' => $kode,
				'nama_makanan' => $d->nama_makanan,
				'jumlah' => $d->qty,
				'harga' => $d->harga,	
				'status' => 'active',	
			);
			$this->db->insert('tb_pesanan',$data);
		
		}
		helper_log('menambah pesanan'. ' ' . $kode ,$user);
		$status = array(
			'status' => true
		);
		echo json_encode($status);
	}
	public function selesai()
	{
		$datajson = $this->input->post('selesai');
		$json = json_decode($datajson);
		$data = array(
			'status' => 'selesai',
		);
		helper_log('Menyelsaikan Pembayaran'. ' ' . $json->kode_pesanan ,$this->session->userdata('user'));
		$this->db->where('kode_pesanan',$json->kode_pesanan);
		$this->db->update('tb_pesanan',$data);
		echo json_encode($json);
	}
	public function get_pesanan()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, OPTIONS");
		$this->db->group_by('kode_pesanan'); 
		$pesanan = $this->db->get_where('tb_pesanan',array('status' => 'active'))->result();
		echo json_encode($pesanan);
	}
	
	public function cek_login()
	{
			$username = $this->input->post('username');
			$pass = 	$this->input->post('password');
			$where = array('username' => $username,'password'=> $pass);
			$cek = $this->db->get_where('tb_user',$where)->num_rows();
			if($cek > 0) {
				$user = $this->db->get_where('tb_user',$where)->row();
				$data = array(

					'status' => TRUE,
					'user' => $user->id_user,
					'level' => $user->id_level,

				);
				helper_log("login",$user->id_user);
				$this->session->set_userdata($data);
				echo json_encode($data);
				redirect('front_end/restoran/menu');

			}else {
				echo json_encode('Null');
			}

	}
	public function count()
	{
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, OPTIONS");
		$this->db->group_by('kode_pesanan'); 
		$pesanan = $this->db->get('tb_pesanan')->result();
		echo json_encode(array('jumlah' => count($pesanan)));

	}

	

}
