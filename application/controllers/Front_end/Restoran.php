<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restoran extends CI_Controller {

	public function Menu()
	{
		$data['content'] = 'menu';
		$this->load->view('front_end',$data);
	}
	public function pesanan()
	{
		
		$data['content'] = 'pesanan_baru';
		$this->load->view('front_end',$data);
	}
	
	public function data_pesanan()
	{	
		$data['content'] = 'data_pesanan';
		$this->load->view('front_end',$data);
	}
	public function data_activity()
	{	
		$data['content'] = 'data_activity';
		$this->load->view('front_end',$data);
	}
	public function pembayaran()
	{
		
		$data['content'] = 'pembayaran';
		$this->load->view('front_end',$data);
	}
	public function del($id){
		$this->load->model('M_menu');
		$this->M_menu->del($id);

		redirect('Front_end/restoran/data_makanan');
	}
	public function tambah_makanan()
	{
		
		$data['content'] = 'tambah_makanan';
		$this->load->view('front_end',$data);
		
	}

	public function data_makanan()
	{
		
		$data['content'] = 'data_makanan';
		$this->load->view('front_end',$data);
	}
	
	public function edit_makanan($id)
	{
		$data['content'] = 'makanan/edit_makanan';
		$data['menu'] = $this->db->get_where('tb_menu_makanan',array('id_menu' => $id))->row();
		$this->load->view('front_end',$data);
	}
	public function bayar($id)
	{
		$this->load->model('M_pesan');
		$data['content'] = 'pembayaran';
		$this->M_pesan->get_pesanan($id);
		$this->load->view('front_end',$data);
	}
	public function tampil_pesan($kode){
		$this->load->model('M_pesan');
		$data['content'] = 'pembayaran';
		$data = $this->M_pesan->get_pesanan($kode);
		echo json_encode($data);
	}

}
