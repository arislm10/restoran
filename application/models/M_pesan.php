<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pesan extends CI_Model {

	public function get_pesanan($kode)
	{
		return $this->db->get_where('tb_pesanan',array('kode_pesanan' => $kode))->result();
    }
}
?>