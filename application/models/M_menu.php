<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_menu extends CI_Model {

	public function insert($data)
	{
		$this->db->insert('tb_menu_makanan',$data);
	}
	public function del($id){
	$this->db->where('id_menu', $id);
	$this->db->delete('tb_menu_makanan');
	}

	public function edit($data,$id) {
		$this->db->where('id_menu', $id);
	$this->db->update('tb_menu_makanan',$data);	
	}
}
?>