<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_log extends CI_Model {

	public function save_log($data)
	{
		$this->db->insert('activity_log',$data);
	}
	
	public function get_log($id)
	{
		return $this->db->get_where('activity_log',array('id_user' => $id));
  }
}
?>