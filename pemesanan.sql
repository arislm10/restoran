/*
SQLyog Ultimate v10.42 
MySQL - 5.5.5-10.1.37-MariaDB : Database - pemesanan
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pemesanan` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pemesanan`;

/*Table structure for table `activity_log` */

DROP TABLE IF EXISTS `activity_log`;

CREATE TABLE `activity_log` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `log` varchar(255) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `tanggal` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

/*Data for the table `activity_log` */

insert  into `activity_log`(`id_log`,`log`,`id_user`,`tanggal`) values (1,'Login',2,'2019-03-09 17:18:24'),(2,'Login',1,'2019-03-09 17:18:51'),(3,'login',2,'2019-03-10 14:35:56'),(4,'login',1,'2019-03-10 16:09:55'),(6,'login',1,'2019-03-10 16:12:44'),(9,'menambah Pesanan \".	$kode.\"',1,'2019-03-10 16:21:12'),(10,'menambah pesanan ERP1022019-0010',1,'2019-03-10 16:25:02'),(11,'login',1,'2019-03-10 16:27:40'),(12,'login',2,'2019-03-10 16:29:34'),(13,'Logout',NULL,'2019-03-10 16:29:35'),(14,'login',2,'2019-03-10 16:30:19'),(15,'Logout',2,'2019-03-10 16:31:45'),(16,'login',2,'2019-03-10 16:32:06'),(17,'menambah pesanan ERP1022019-0010',2,'2019-03-10 16:36:41'),(18,'Menyelsaikan Pembayaran ERP1022019-005',2,'2019-03-10 16:37:19'),(19,'Menyelsaikan Pembayaran ERP1022019-006',2,'2019-03-10 16:37:24'),(20,'Menyelsaikan Pembayaran ERP1022019-007',2,'2019-03-10 16:38:41'),(21,'menambah pesanan ERP1022019-011',2,'2019-03-10 16:39:05'),(22,'Menyelsaikan Pembayaran ERP1022019-008',2,'2019-03-10 16:39:27'),(23,'menambah menu baru',2,'2019-03-10 17:21:44'),(24,'menambah menu baru',2,'2019-03-10 17:23:24'),(25,'Logout',2,'2019-03-10 17:29:37'),(26,'login',2,'2019-03-10 17:30:03'),(27,'login',2,'2019-03-11 03:13:28'),(28,'login',2,'2019-03-11 08:18:14'),(29,'login',1,'2019-03-11 16:18:16'),(30,'menambah menu baru',1,'2019-03-11 16:34:19'),(31,'menambah menu baru',1,'2019-03-11 16:34:24'),(32,'edit Makanan',1,'2019-03-11 16:34:49'),(33,'edit Makanan',1,'2019-03-11 16:34:55'),(34,'Logout',1,'2019-03-11 16:37:43'),(35,'login',1,'2019-03-11 16:37:50'),(36,'Logout',1,'2019-03-11 16:38:00'),(37,'login',2,'2019-03-11 16:38:05'),(38,'Logout',2,'2019-03-11 16:38:56'),(39,'login',2,'2019-03-11 16:39:00'),(40,'Logout',2,'2019-03-11 16:42:12'),(41,'login',2,'2019-03-11 16:42:17'),(42,'Logout',2,'2019-03-11 16:42:36'),(43,'login',1,'2019-03-11 16:42:44'),(44,'Logout',1,'2019-03-11 16:44:18'),(45,'login',2,'2019-03-11 16:44:26'),(46,'Logout',2,'2019-03-11 16:44:28'),(47,'login',2,'2019-03-11 16:53:39');

/*Table structure for table `tb_level` */

DROP TABLE IF EXISTS `tb_level`;

CREATE TABLE `tb_level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `hak_akses` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tb_level` */

insert  into `tb_level`(`id_level`,`hak_akses`) values (1,'Kasir'),(2,'Pelayan');

/*Table structure for table `tb_menu_makanan` */

DROP TABLE IF EXISTS `tb_menu_makanan`;

CREATE TABLE `tb_menu_makanan` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `nama_makanan` varchar(50) DEFAULT NULL,
  `status` enum('ready','kosong') DEFAULT NULL,
  `harga` int(50) DEFAULT NULL,
  `desc` varchar(100) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `tb_menu_makanan` */

insert  into `tb_menu_makanan`(`id_menu`,`nama_makanan`,`status`,`harga`,`desc`,`qty`) values (1,'Roti Tawar2','ready',260000,'Roti yang terbuat dari Ikan',1),(2,'Pizza','ready',340000,'Pizza yang terbuat dari Ikan',1),(3,'Es Teh','ready',5000,'Nikmat',1),(6,'Jus Jambu','ready',1500,'terbuat dari nanas',1);

/*Table structure for table `tb_pesanan` */

DROP TABLE IF EXISTS `tb_pesanan`;

CREATE TABLE `tb_pesanan` (
  `id_pesanan` int(11) NOT NULL AUTO_INCREMENT,
  `kode_pesanan` varchar(50) DEFAULT NULL,
  `nama_makanan` varchar(255) DEFAULT NULL,
  `jumlah` varchar(30) DEFAULT NULL,
  `harga` int(100) DEFAULT NULL,
  `status` enum('active','selesai') DEFAULT NULL,
  `no_meja` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_pesanan`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

/*Data for the table `tb_pesanan` */

insert  into `tb_pesanan`(`id_pesanan`,`kode_pesanan`,`nama_makanan`,`jumlah`,`harga`,`status`,`no_meja`) values (13,'ERP03102019-001','Es Jeruk','2',5000,'selesai',3),(14,'ERP03102019-001','Susu','2',1500,'selesai',3),(15,'ERP110919908-002','Susu','1',1500,'active',2),(21,'ERP110919908-002','Es Teh','1',5000,'active',2),(25,'ERP1022019-003','Es Jeruk','1',5000,'selesai',3),(26,'ERP1022019-003','Es Teh','2',5000,'selesai',3),(27,'ERP1022019-004','Pizza','2',340000,'selesai',5),(28,'ERP1022019-004','Susu','3',1500,'selesai',5),(29,'ERP1022019-005','Roti Tawar','1',260000,'selesai',1),(30,'ERP1022019-006','Pizza','1',340000,'selesai',1),(31,'ERP1022019-006','Pizza','1',340000,'selesai',1),(32,'ERP1022019-006','Pizza','1',340000,'selesai',1),(33,'ERP1022019-006','Susu','1',1500,'selesai',1),(34,'ERP1022019-007','Susu','2',1500,'selesai',2),(35,'ERP1022019-008','Es Jeruk','2',5000,'selesai',1),(36,'ERP1022019-008','Susu','2',1500,'selesai',1),(37,'ERP1022019-009','Susu','1',1500,'active',2),(38,'ERP1022019-0010','Es Jeruk','1',5000,'selesai',1),(39,'ERP1022019-0010','Susu','1',1500,'selesai',1),(40,'ERP1022019-011','Es Jeruk','1',5000,'active',2),(41,'ERP1022019-011','Es Teh','1',5000,'active',2);

/*Table structure for table `tb_user` */

DROP TABLE IF EXISTS `tb_user`;

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `id_level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tb_user` */

insert  into `tb_user`(`id_user`,`username`,`password`,`id_level`) values (1,'pelayan','pelayan',1),(2,'kasir','kasir',2);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
